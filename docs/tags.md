# 🏷️ Tags

Cette page n'est pas indispensable, vous pouvez la supprimer.

Elle permet toutefois de lister automatiquement les tags que vous auriez ajouté au début de vos pages.